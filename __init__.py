import matplotlib as mpl
from subprocess import call
import matplotlib.pyplot as plt

header = """\\documentclass[a4paper, 11pt]{article}
\\usepackage{fontspec}
\\usepackage{lmodern}
\\usepackage{graphicx}
%\\usepackage[french]{babel}
%\\usepackage{physics}
%\\usepackage{enumerate}
%\\usepackage{amsmath,amsthm,amsfonts,amssymb}

\\begin{document}
"""

class TexFile:
    def __init__(self, f = 'act.tex'):
        self.filename = f
        f = open(f, 'r')
        self.main = self.get_doc(f.read())
        f.close()
        #self.f.write(header)
        self._dirsave = './figure/'
        mplparam = {
                'axes.titlesize' : 24,
                'axes.labelsize' : 20,
                'lines.linewidth' : 3,
                'lines.markersize' : 10,
                'xtick.labelsize' : 16,
                'ytick.labelsize' : 16
                }
        mpl.rc(mplparam)
        self.mplparams = mpl.rcParams

    def get_doc(self, t):
        start = t.index('\\begin{document}')+16
        stop = t.index('\\end{document}')
        t = t[start:stop].strip()
        f = t.split('\n%---')
        return f


    @property
    def dirsave(self):
        return self._dirsave

    @dirsave.setter
    def dirsave(self, val):
        if val[-1] == '/':
            self._dirsave = val
        else :
            self._dirsave = val + '/'

    def sep(self):
        '%--- l'
        pass
    
    def writefig(self, name, fig=None):
        p = self._dirsave + name
        if fig is None:
            plt.savefig(p, format='ps')
        else:
            fig.set_size_inches(16, 9)
            fig.savefig(p, format='ps')
        self.f.write('\includegraphics[width=0.9\linewidth]{{{}}}\n\n'.format(p))

    def wtex(self, text):
        self.f.write(text)
        self.f.write('\n')

    def compile(self):
        call(["latexmk", self.filename])

    def close(self):
        self.f.write('\\end{document}\n')
        self.f.close()
        self.compile()


